package ro.licenta.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ro.licenta.model.User;

@Repository("userRepository")
public interface UserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User>{
	
	@Query("SELECT p FROM User p WHERE p.username = :username")
	User findByName(@Param("username") String username);
	
	User findByEmail(@Param("email") String email);
	
	@Query("SELECT p FROM User p WHERE p.username LIKE :searchTerm OR p.email LIKE :searchTerm")
	public List<User> search(@Param("searchTerm") String searchTerm);
}
