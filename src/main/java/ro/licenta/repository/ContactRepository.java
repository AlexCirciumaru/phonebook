package ro.licenta.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import ro.licenta.model.Contact;

@Repository("contactRepository")
public interface ContactRepository extends JpaRepository<Contact, Long>, JpaSpecificationExecutor<Contact>{

	public List<Contact> findByUserId(Long id);
}
