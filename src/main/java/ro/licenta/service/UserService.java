package ro.licenta.service;

import java.util.List;

import ro.licenta.model.User;

public interface UserService {
	
	User findUser(Long userId);
	
	User findUser(String username);
	
	List<User> findAllUsers();
	
	User add(User user);
	
	User update(User user);
	
	void delete(Long userId);
}
