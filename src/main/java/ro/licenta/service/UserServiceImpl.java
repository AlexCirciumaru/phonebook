package ro.licenta.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ro.licenta.model.User;
import ro.licenta.repository.UserRepository;
import ro.licenta.exception.DuplicateRecordException;
import ro.licenta.exception.RecordNotFoundException;

@Service("userService")
public class UserServiceImpl implements UserService{
	
	private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
	
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public User findUser(Long userId) {
		// TODO Auto-generated method stub
		return userRepository.findById(userId).orElse(null);
	}

	@Override
	public User findUser(String username) {
		// TODO Auto-generated method stub
		return userRepository.findByName(username);
	}

	@Override
	public List<User> findAllUsers() {
		// TODO Auto-generated method stub
		return userRepository.findAll();
	}

	@Override
	@Transactional
	public User add(User user) {
		// TODO Auto-generated method stub
		//Check if already exist a user with same email
		User existingUser = userRepository.findByEmail(user.getEmail());
		if(existingUser != null) {
			String errorMessage = "Already exists a user with same email: " + user.getEmail();
			logger.error(errorMessage);
			throw new DuplicateRecordException(errorMessage);
		}
		userRepository.save(user);
		return user;
	}

	@Override
	@Transactional
	public User update(User user) {
		// TODO Auto-generated method stub
		User existingUser = userRepository.findById(user.getId()).orElse(null);
		if(existingUser == null) {
			String errorMessage = "User with id " + user.getId() + " not found";
			logger.error(errorMessage);
			throw new RecordNotFoundException(errorMessage);
		}
		
		//Check if email was changed
		if(!existingUser.getEmail().equals(user.getEmail())) {
			//Email changed, check again if new email already exists
			if(userRepository.findByEmail(user.getEmail()) != null) {				
				String errorMessage = "The new email address is already used by other user: " + user.getEmail();
				logger.error(errorMessage);
				throw new DuplicateRecordException(errorMessage);
			}
		}
		
		if(user.getPassword() == null) {
			//if password is null keep existing password
			user.setPassword(existingUser.getPassword());
		}
		return userRepository.save(user);
	}

	@Override
	@Transactional
	public void delete(Long userId) {
		// TODO Auto-generated method stub
		User user = userRepository.findById(userId).orElse(null);
		logger.debug("Delete user with id: " + userId);
		if(user != null) {
			userRepository.delete(user);
		} else {
			String errorMessage = "User with id " + userId + " not found";
			logger.error(errorMessage);
			throw new RecordNotFoundException(errorMessage);
		}
	}
}
