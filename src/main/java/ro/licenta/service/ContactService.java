package ro.licenta.service;

import java.util.List;

import ro.licenta.model.Contact;

public interface ContactService {
	
	Contact findContact(Long contactId);
	
	List<Contact> findAllContacts();
	
	List<Contact> findUserContacts(Long userId);
	
	Contact add(Contact contact);
	
	Contact update(Contact contact);
	
	void delete(Long contactId);
}
