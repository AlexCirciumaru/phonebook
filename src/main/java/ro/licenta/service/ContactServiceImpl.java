package ro.licenta.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ro.licenta.exception.RecordNotFoundException;
import ro.licenta.model.Contact;
import ro.licenta.repository.ContactRepository;

@Service("contactService")
public class ContactServiceImpl implements ContactService{
	
	private static final Logger logger = LoggerFactory.getLogger(ContactServiceImpl.class);
	
	@Autowired
	private ContactRepository contactRepository;
	
	@Override
	public Contact findContact(Long contactId) {
		// TODO Auto-generated method stub
		Contact contact = contactRepository.findById(contactId).orElse(null);
		return contact;
	}

	@Override
	@Transactional(readOnly = true)
	public List<Contact> findAllContacts() {
		// TODO Auto-generated method stub
		return contactRepository.findAll();
	}

	@Override
	@Transactional(readOnly = true)
	public List<Contact> findUserContacts(Long userId) {
		// TODO Auto-generated method stub
		return contactRepository.findByUserId(userId);
	}
	
	@Override
	@Transactional
	public Contact add(Contact contact) {
		// TODO Auto-generated method stub
		contactRepository.save(contact);
		return contact;
	}

	@Override
	@Transactional
	public Contact update(Contact contact) {
		// TODO Auto-generated method stub
		Contact existingContact = contactRepository.findById(contact.getId()).orElse(null);
		if(existingContact == null) {
			String errorMessage = "Contact with id " + contact.getId() + " not found";
			logger.error(errorMessage);
			throw new RecordNotFoundException(errorMessage);
		}
		return contactRepository.save(contact);
	}

	@Override
	@Transactional
	public void delete(Long contactId) {
		// TODO Auto-generated method stub
		Contact contact = contactRepository.findById(contactId).orElse(null);
		logger.debug("Delete contact with id: " + contactId);
		if(contact != null) {
			contactRepository.deleteById(contactId);
		} else {
			String errorMessage = "Contact with id " + contactId + " not found";
			logger.error(errorMessage);
			throw new RecordNotFoundException(errorMessage);
		}
	}
}
