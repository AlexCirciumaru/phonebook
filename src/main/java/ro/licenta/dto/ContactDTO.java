package ro.licenta.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class ContactDTO {
	
	private Long id;
	
	@NotBlank(message = "The first name can not be empty")
	private String firstName;
	
	@NotBlank(message = "The last name can not be empty")
	private String lastName;
	
	@NotBlank(message = "The phone number can not be empty")
	private String phoneNumber;
	
	@NotBlank(message = "The address can not be empty")
	private String address;
	
	@NotNull
	private Long userId;
	
	public ContactDTO() {		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}
}
