package ro.licenta.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class UserDTO {
	
	private Long id;
	
	@NotBlank(message = "The username can not be empty")
	@Size(max = 64, message = "The username can not exceed 64 characters")
	private String username;
	
	@NotBlank(message = "The user email can not be empty")
	@Email(message = "The user email is invalid")
	private String email;
	
	private String password;
	
	public UserDTO() {		
	}
	
	public UserDTO(Long id) {
		this.id = id;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
