package ro.licenta.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ValidationUtils;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ro.licenta.model.Contact;
import ro.licenta.model.User;
import ro.licenta.service.ContactService;
import ro.licenta.service.UserService;
import ro.licenta.dto.UserDTO;
import ro.licenta.exception.DuplicateRecordException;

@Controller
public class UserController {
	
	private final Logger logger = LoggerFactory.getLogger(UserController.class);
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ContactService contactService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@RequestMapping(value = "/users", method = RequestMethod.GET)
	public String getUsers(Model model) {
		List<User> users = userService.findAllUsers();
		model.addAttribute("users", users);
		return "users";
	}
	
	@RequestMapping(value = "/user/{userId}/contacts", method = RequestMethod.GET)
	public String getUserContacts(@PathVariable("userId") Long userId, Model model) {
		List<Contact> contacts = contactService.findUserContacts(userId);
		model.addAttribute("contacts", contacts);
		return "contacts";
	}
	
	@RequestMapping(value = "/user/add", method = RequestMethod.GET)
	public String getAddUserForm(Model model) {
		UserDTO user = new UserDTO();
		model.addAttribute("user", user);
		return "add-user";
	}
	
	/**
	   * This method will be called on form submission, handling POST request for
	   * add user in database. It also validates the user input
	   */
	@RequestMapping(value = "/user/add", method = RequestMethod.POST)
	public String addUserForm(@Valid @ModelAttribute("user") UserDTO user, BindingResult result, RedirectAttributes redirectAttributes) {
		ValidationUtils.rejectIfEmptyOrWhitespace(result, "password", "notEmpty.password", "Password can not be empty");
		if(result.hasErrors()) {
			logger.error("Add user error: {}", result.getAllErrors());
			return "add-user";
		} else {
			try {
				User userToAdd = new User();
				userToAdd.setUsername(user.getUsername());
				userToAdd.setEmail(user.getEmail());
				//Before save encode password.
				userToAdd.setPassword(passwordEncoder.encode(user.getPassword()));
				userService.add(userToAdd);
			} catch(DuplicateRecordException e) {
				result.rejectValue("email", "duplicate", "Email already used");
				logger.error("Add user error: " + result.getAllErrors());
				return "add-user";
			}
			redirectAttributes.addFlashAttribute("message", "Successfully added!");
			return "redirect:/users";
		}	
	}
	
	@RequestMapping(value = "/user/update", method = RequestMethod.GET)
	public String getEditUserForm(Model model, @RequestParam(value = "id", required = true) Long id, RedirectAttributes redirectAttributes) {
		User existingUser = userService.findUser(id);
		if(existingUser != null) {
			UserDTO user = new UserDTO();
			user.setId(existingUser.getId());
			user.setUsername(existingUser.getUsername());
			user.setEmail(existingUser.getEmail());
			user.setPassword(null); //Do not sent password.
			model.addAttribute("user", user);
			return "update-user";
		} else {
			redirectAttributes.addFlashAttribute("errorMessage", "User not found");
			return "redirect:/users";
		}
	}
	
	@RequestMapping(value = "/user/update", method = RequestMethod.POST)
	public String updateUser(@Valid @ModelAttribute("user") UserDTO user, BindingResult result) {
		if(result.hasErrors()) {
			logger.error("Update user error: " + result.getAllErrors());
			return "update-user";
		} else {
			try {
				User userToUpdate = new User();
				userToUpdate.setId(user.getId());
				userToUpdate.setUsername(user.getUsername());
				userToUpdate.setEmail(user.getEmail());
				if(user.getPassword() != null && !user.getPassword().trim().isEmpty()) {
					//If password is not null encode password.
					userToUpdate.setPassword(passwordEncoder.encode(user.getPassword()));
				} else {
					user.setPassword(null);
				}
				
				userService.update(userToUpdate);
				
			}catch(DuplicateRecordException e) {
				result.rejectValue("email", "duplicate", "New email address already used by other user.");
				logger.error("Update user error: " + result.getAllErrors());
				return "update-user";
			}
			return "redirect:/users";
		}
	}
	
	@RequestMapping(value = "/user/delete", method = RequestMethod.GET)
	public String deleteUser(@Valid @ModelAttribute("id") Long id,
			BindingResult result, RedirectAttributes redirectAttributes) {
		try {
			userService.delete(id);
			redirectAttributes.addFlashAttribute("message", "Successfully deleted.");
		} catch (DataIntegrityViolationException e) {
			String errorMessage = "Can not delete user because is assigned";
			logger.error(errorMessage);
			redirectAttributes.addFlashAttribute("errorMessage", errorMessage);
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("errorMessage", "Delete error: " + e.getMessage());
		}
		return "redirect:/users";
	}
}
