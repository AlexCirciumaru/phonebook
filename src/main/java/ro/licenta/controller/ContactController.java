package ro.licenta.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ro.licenta.dto.ContactDTO;
import ro.licenta.model.Contact;
import ro.licenta.model.User;
import ro.licenta.service.ContactService;
import ro.licenta.service.UserService;

@Controller
public class ContactController {

	private final Logger logger = LoggerFactory.getLogger(ContactController.class);
	
	@Autowired
	private ContactService contactService;
	
	@Autowired
	private UserService userService;
	
	@RequestMapping(value = "/contacts", method = RequestMethod.GET)
	public String getContacts(Model model) {
		List<Contact> contacts = contactService.findAllContacts();
		model.addAttribute("contacts", contacts);
		return "contacts";
	}
	
	@RequestMapping(value = "/contact/add", method = RequestMethod.GET)
	public String getAddContactForm(Model model) {
		//Add list of users to model to populate the users selector.
		List<User> users = userService.findAllUsers();
		model.addAttribute("users", users);
		
		ContactDTO contact = new ContactDTO();
		model.addAttribute("contact", contact);
		return "add-contact";
	}
	
	/**
	   * This method will be called on form submission, handling POST request for
	   * saving task in database. It also validates the task input
	   */
	@RequestMapping(value = "/contact/add", method = RequestMethod.POST)
	public String addContactForm(@Valid @ModelAttribute("contact") ContactDTO contactDTO,
			BindingResult result, Model model, RedirectAttributes redirectAttributes) {
		if(result.hasErrors()) {
			logger.error("Add contact error: " + result.getAllErrors());
			//Add list of users to model to populate the users selector.
			List<User> users = userService.findAllUsers();
			model.addAttribute("users", users);
			return "add-contact";
		} else {
			Contact contact = new Contact();
			contact.setFirstName(contactDTO.getFirstName());
			contact.setLastName(contactDTO.getLastName());
			contact.setPhoneNumber(contactDTO.getPhoneNumber());
			contact.setAddress(contactDTO.getAddress());
			contact.setUser(new User(contactDTO.getUserId()));
			contactService.add(contact);
			redirectAttributes.addFlashAttribute("message", "Successfully added.");
			return "redirect:/contacts";
		}
	}
	
	@RequestMapping(value = "/contact/update", method = RequestMethod.GET)
	public String getEditContactForm(@RequestParam(value = "id", required = true) Long id,
			Model model, RedirectAttributes redirectAttributes) {
		Contact contact = contactService.findContact(id);
		logger.debug("Edit Contact {}", contact);
		if(contact != null) {
			//Create and put a ContactDTO needed to edit contact.
			ContactDTO contactDTO = new ContactDTO();
			contactDTO.setId(contact.getId());
			contactDTO.setFirstName(contact.getFirstName());
			contactDTO.setLastName(contact.getLastName());
			contactDTO.setPhoneNumber(contact.getPhoneNumber());
			contactDTO.setAddress(contact.getAddress());
			contactDTO.setUserId(contact.getUser().getId());
			
			model.addAttribute("contact", contactDTO);
			
			// Add to model supplementary attributes needed to construct the edit form.
		    // Add list of users to model to populate the users selector.
			List<User> users = userService.findAllUsers();
			model.addAttribute("users", users);
			return "update-contact";
		} else {
			logger.error("Edit error: Contact with id {} not found", id);
			redirectAttributes.addFlashAttribute("errorMessage", "Contact with specified id not found.");
			return "redirect:/contacts";
		}
	}
	
	@RequestMapping(value = "/contact/update", method = RequestMethod.POST)
	public String updateContact(@Valid @ModelAttribute("contact") ContactDTO contactDTO,
			BindingResult result, Model model, RedirectAttributes redirectAttributes) {
		if(result.hasErrors()) {
			logger.error("Update contact validation error: "+ result.getAllErrors());
			// Add to model supplementary attributes needed to construct the edit form.
		    // Add list of users to model to populate the users selector.
		    List<User> users = userService.findAllUsers();
		    model.addAttribute("users", users);
		    
		    return "update-contact";
		} else {
			Contact contact = new Contact();
			contact.setId(contactDTO.getId());
			contact.setFirstName(contactDTO.getFirstName());
			contact.setLastName(contactDTO.getLastName());
			contact.setPhoneNumber(contactDTO.getPhoneNumber());
			contact.setAddress(contactDTO.getAddress());
			contact.setUser(new User(contactDTO.getUserId()));
			
			contactService.update(contact);
			
			return "redirect:/contacts";
		}
	}
	
	@RequestMapping(value = "/contact/delete", method = RequestMethod.GET)
	public String deleteContact(@Valid @ModelAttribute("id") Long id,
			BindingResult result, RedirectAttributes redirectAttributes) {
		try {
			contactService.delete(id);
			redirectAttributes.addFlashAttribute("message", "Successfully deleted.");
		} catch (Exception e) {
			redirectAttributes.addFlashAttribute("errorMessage", "Delete error: " + e.getMessage());
		}
		
		return "redirect:/contacts";
	}
}
