<%@ include file="header.jsp" %>
<!-- Page content (Begin)  -->

	<div class="container myrow-container">
		<div class="panel panel-success">
			<div class="panel-heading">
				<div align="left">
					<h3 class="panel-title">
						<b>Users List</b>
					</h3>
				</div>
				<div align="right">
					<h3 class="panel-title">
						<a href="<c:url value='/contacts'/>">View Contacts List</a> | <a href="<c:url value='/user/add'/>">Add New User</a>
					</h3>
				</div>
			</div>
			<div class="panel-body">
				<c:if test="${empty users}">
                There are no Users
                <form action="search">
						<div class="row">
							<div class="col-md-6">
								<div class="col-md-6">Search Users:</div>
								<div class="col-md-6">
									<input type="text" name="userName" id="userName">
								</div>
							</div>
							<div class="col-md-4">
								<input class="btn btn-success" type='submit' value='Search' />
							</div>
						</div>
					</form>
				</c:if>
				<c:if test="${not empty users}">

					<form action="search">
						<div class="row">
							<div class="col-md-6">
								<div class="col-md-6">Search Users:</div>
								<div class="col-md-6">
									<input type="text" name="userName" id="userName">
								</div>
							</div>
							<div class="col-md-4">
								<input class="btn btn-success" type='submit' value='Search' />
							</div>
						</div>
					</form>

					<table class="table table-hover table-bordered">
						<thead style="background-color: #bce8f1;">
							<tr>
								<th>Id</th>
								<th>Username</th>
								<th>Email</th>
								<th>Edit</th>
								<th>Delete</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${users}" var="user">
								<tr>
									<th><c:out value="${user.id}" /></th>
									<th><c:out value="${user.username}" /></th>
									<th><c:out value="${user.email}" /></th>
									<th><a href="<c:url value='/user/update?id=${user.id}'/>">Edit</a></th>
									<th><a href="<c:url value='/user/delete?id=${user.id}'/>">Delete</a></th>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</c:if>
			</div>
		</div>
	</div>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>	
<!-- Page content (End)    -->
<%@ include file="footer.jsp" %>		
