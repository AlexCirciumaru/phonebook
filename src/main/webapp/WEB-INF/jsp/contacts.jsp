<%@ include file="header.jsp" %>
<!-- Page content (Begin)  -->

<div class="container myrow-container">
		<div class="panel panel-success">
			<div class="panel-heading">
				<div align="left">
					<h3 class="panel-title">
						<b>Contacts List</b>
					</h3>
				</div>
				<div align="right">
					<h3 class="panel-title">
						<a href="<c:url value='/contact/add'/>">Add New Contact</a> | <a href="<c:url value='/users'/>">View Users List</a>
					</h3>
				</div>
			</div>
			<div class="panel-body">
				<c:if test="${empty contacts}">
                There are no Contacts
                <form action="searchContact">
						<div class="row">
							<div class="col-md-6">
								<div class="col-md-6">Search Contacts:</div>
								<div class="col-md-6">
									<input type="text" name="firstName" id="firstName">
								</div>
							</div>
							<div class="col-md-4">
								<input class="btn btn-success" type='submit' value='Search' />
							</div>
						</div>
					</form>
				</c:if>
				<c:if test="${not empty contacts}">

					<form action="searchContact">
						<div class="row">
							<div class="col-md-6">
								<div class="col-md-6">Search Contacts:</div>
								<div class="col-md-6">
									<input type="text" name="firstName" id="firstName">
								</div>
							</div>
							<div class="col-md-4">
								<input class="btn btn-success" type='submit' value='Search' />
							</div>
						</div>
					</form>

					<table class="table table-hover table-bordered">
						<thead style="background-color: #bce8f1;">
							<tr>
								<th>Id</th>
								<th>First Name</th>
								<th>Last Name</th>
								<th>Phone Number</th>
								<th>Address</th>
								<th>Assigned User</th>
								<th>Edit</th>
								<th>Delete</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${contacts}" var="contact">
								<tr>
									<th><c:out value="${contact.id}" /></th>
									<th><c:out value="${contact.firstName}" /></th>
									<th><c:out value="${contact.lastName}" /></th>
									<th><c:out value="${contact.phoneNumber}" /></th>
									<th><c:out value="${contact.address}" /></th>
									<th><c:out value="${contact.user.username}" /></th>
									<th><a href="<c:url value='/contact/update?id=${contact.id}'/>">Edit</a></th>
									<th><a href="<c:url value='/contact/delete?id=${contact.id}'/>">Delete</a></th>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</c:if>
			</div>
		</div>
	</div>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script
		src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<!-- Page content (End)    -->
<%@ include file="footer.jsp" %>