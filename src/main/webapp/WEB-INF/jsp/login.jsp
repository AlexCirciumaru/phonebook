<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<style>
body {
	display: -ms-flexbox;
	display: flex;
	-ms-flex-align: center;
	align-items: center;
	padding-top: 40px;
	padding-bottom: 40px;
	background-color: #f5f5f5;
}

.form-signin {
	width: 100%;
	max-width: 330px;
	padding: 15px;
	margin: auto;
}

.form-signin .form-control {
	position: relative;
	box-sizing: border-box;
	height: auto;
	padding: 10px;
	font-size: 16px;
}

.form-signin .form-control:focus {
	z-index: 2;
}

.form-signin input[type="text"] {
	margin-bottom: -1px;
	border-bottom-right-radius: 0;
	border-bottom-left-radius: 0;
}

.form-signin input[type="password"] {
	margin-bottom: 10px;
	border-top-left-radius: 0;
	border-top-right-radius: 0;
}

.btn {
	background-color: #007BFF;
	display: block;
	color: white;
	border-radius: 5px;
	width: 220px;
	height: 50px;
	border-style: solid;
	font-family: "Segoe UI", Frutiger, "Frutiger Linotype", "Dejavu Sans",
		"Helvetica Neue", Arial, sans-serif;
	font-size: 20px;
}

.btn:hover {
	background-color: #095fbc;
}
</style>

<form class="form-signin" action="login" method="POST">
	<h1 class="h3 mb-3 font-weight-normal">Please Sign In</h1>
	<label for="inputLoginname" class="sr-only"></label> <input type="text"
		id="username" name="username" class="form-control"
		placeholder="Login Name" required autofocus /> <label
		for="inputPassword" class="sr-only"></label> <input type="password"
		id="password" name="password" class="form-control"
		placeholder="Password" required autofocus />
	<button class="btn" type="submit">Sign In</button>
	<p class="mt-5 mb-3 text-muted">&copy; 2018-2019</p>
</form>
	<p class="mt-5 mb-3 text-muted"><a href="<c:url value='/user/add'/>"> Register </a></p>