<%@ include file="header.jsp" %>
<!-- Page content (Begin)  -->

<div class="container myrow-container">
	<div class="panel panel-success">
		<div class="panel-heading">
			<h1>Update contact</h1>
		</div>
		<div class="panel-body">
			<form:form cssClass="form-horizontal" method="post"
				modelAttribute="contact">
				
				<div class="form-group">
					<div class="control-label col-xs-3">
						<form:label path="firstName">First Name</form:label>
					</div>
					<div class="col-xs-6">
						<form:hidden path="id" value="${contactObject.id}" />
						<form:input cssClass="form-control" path="firstName"
							value="${contactObject.firstName}" />
					</div>
				</div>
				
				<div class="form-group">
					<div class="control-label col-xs-3">
						<form:label path="lastName">Last Name</form:label>
					</div>
					<div class="col-xs-6">
						<form:hidden path="id" value="${contactObject.id}" />
						<form:input cssClass="form-control" path="lastName"
							value="${contactObject.lasstName}" />
					</div>
				</div>
				
				<div class="form-group">
					<div class="control-label col-xs-3">
						<form:label path="phoneNumber">Phone Number</form:label>
					</div>
					<div class="col-xs-6">
						<form:hidden path="id" value="${contactObject.id}" />
						<form:input cssClass="form-control" path="phoneNumber"
							value="${contactObject.phoneNumber}" />
					</div>
				</div>
				
				<div class="form-group">
					<div class="control-label col-xs-3">
						<form:label path="address">Address</form:label>
					</div>
					<div class="col-xs-6">
						<form:hidden path="id" value="${contactObject.id}" />
						<form:input cssClass="form-control" path="address"
							value="${contactObject.address}" />
					</div>
				</div>
				
				<div class="form-group">
					<div class="control-label col-xs-3">
						<form:label path="userId">Assigned User:</form:label>
					</div>
					<div class="col-xs-6">
						<form:select path="userId" required="true">
						<form:option value="">--Select--</form:option>	
						<form:options items="${users}" itemLabel="username" itemValue="id" />			
					</form:select>
					</div>
				</div>
				
				<div class="form-group">
					<div class="row">
						<div class="col-xs-4"></div>
						<div class="col-xs-4">
							<input type="submit" class="btn btn-primary" value="Update" />
						</div>
						<div class="col-xs-4"></div>
					</div>
				</div>
				<form:errors path="*" cssClass="errorblock" element="div" />
			</form:form>
		</div>
	</div>
</div>

<!-- Page content (End)    -->
<%@ include file="footer.jsp" %>