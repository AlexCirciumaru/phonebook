<%@ include file="header.jsp"%>
<!-- Page content (Begin)  -->

<div class="container myrow-container">
	<div class="panel panel-success">
		<div class="panel-heading">
			<h1>Add user</h1>
		</div>
		<div class="panel-body">
			<form:form cssClass="form-horizontal" method="post"
				modelAttribute="user">

				<div class="form-group">
					<div class="control-label col-xs-3">
						<form:label path="username">User Name</form:label>
					</div>
					<div class="col-xs-6">
						<form:hidden path="id" value="${userObject.id}" />
						<form:input cssClass="form-control" path="username"
							value="${userObject.username}" />
					</div>
				</div>

				<div class="form-group">
					<form:label path="password" cssClass="control-label col-xs-3">Password</form:label>
					<div class="col-xs-6">
						<form:hidden path="id" value="${userObject.id}" />
						<form:input cssClass="form-control" path="password"
							value="${userObject.password}" />
					</div>
				</div>

				<div class="form-group">
					<div class="control-label col-xs-3">
						<form:label path="email">Email</form:label>
					</div>
					<div class="col-xs-6">
						<form:hidden path="id" value="${userObject.id}" />
						<form:input cssClass="form-control" path="email"
							value="${userObject.email}" />
					</div>
				</div>

				<div class="form-group">
					<div class="row">
						<div class="col-xs-4"></div>
						<div class="col-xs-4">
							<input type="submit" class="btn btn-primary" value="Add" />
						</div>
						<div class="col-xs-4"></div>
					</div>
				</div>
				<form:errors path="*" cssClass="errorblock" element="div" />
			</form:form>
		</div>
	</div>
</div>

<!-- Page content (End)    -->
<%@ include file="footer.jsp"%>